unit UDeathEnemy;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math, uenemy, std, typinfo;

type
  TDeathEnemy = class(TEnemy)
  public
    constructor Create(); override;
    function MakeMove(Characters: TMatrix; CharacterAlive: array of boolean;
      w, h: integer): TDir; override;
  end;

implementation

constructor TDeathEnemy.Create;
begin
  inherited;
  FBitMapFile := 'Pictures/deathe.bmp';
  LoadBitMap;
end;

function TDeathEnemy.MakeMove(Characters: TMatrix; CharacterAlive: array of boolean;
  w, h: integer): TDir;
var
  t: integer;
begin
  t := random(4);
  Result := TDir(t);
end;

initialization
  ClassE[MaxE] := TDeathEnemy;
  Inc(MaxE);

end.
