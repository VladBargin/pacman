unit ugamemech;

{$mode objfpc}{$H+}

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Math, GMap, UGamePanel, UGameCharacter, std,
  uenemy, uartifact, AnimatedGif, MemBitmap;

type
  TGameMech = class(TPanel)
  public
    WidthB: integer;
    HeightB: integer;
    CellWidth: integer;
    CellHeight: integer;
    XShift: integer;
    YShift: integer;

    Characters: TMatrix;
    CharacterAlive: array of boolean;

    Enemies: TMatrix;
    EnemyAlive: array of boolean;


    Cells: array of array of TGameCell;

    CanRefresh: boolean;
    procedure Refresh;
    procedure ForcedRefresh;

    procedure CreateBoard(w, h, cw, ch, sx, sy: integer);
    procedure AddCharacter(gamec: TGameCharacter; x, y: integer);
    procedure AddEnemy(enemy: TEnemy; x, y: integer);
    procedure AddArtifact(artifact: TArtifact; x, y: integer);

    procedure ShiftCharacter(gamec: TGameCharacter; dir: TDir);
    procedure MoveCharacter(gamec: TGameCharacter; x, y: integer);

    procedure CreateRandom(x, y, i, j: integer);
    constructor Create(AOwner: TComponent);

    procedure SetEndGameCell(x, y: integer);
    procedure CheckForEndGame;

    procedure HandleDeath(var Msg: TMessage); message WM_DEAD;
    procedure KillCharacter(id: integer);
    procedure DisposeOfCharacter(id: integer);
  private


    EndCell: array[0..1] of integer;
    PlayerInd: integer;
    PlayerCount: integer;

    EnemyInd: integer;
    EnemyCount: integer;

    TurnsDone: integer;
    TurnDone: array of boolean;

    procedure DoAITurn;
    procedure ActivateArtifacts;
  end;

implementation

procedure TGameMech.Refresh;
var
  i, j: integer;
begin
  if CanRefresh then
    for i := 0 to WidthB - 1 do
      for j := 0 to HeightB - 1 do
        Cells[i][j].Paint;
end;

procedure TGameMech.ForcedRefresh;
var
  i, j: integer;
begin
  if CanRefresh then
    for i := 0 to WidthB - 1 do
      for j := 0 to HeightB - 1 do
        Cells[i][j].UpdateBg := True;
end;

procedure TGameMech.CreateRandom(x, y, i, j: integer);
var
  p: TGameCell;
begin
  Cells[i][j] := TGameCell(ClassGC[random(MaxGc)].Create(Parent));
  Cells[i][j].Left := x;
  Cells[i][j].Top := y;
  Cells[i][j].Width := 70;
  Cells[i][j].Height := 70;
  Cells[i][j].Show;
end;

constructor TGameMech.Create(AOwner: TComponent);
begin
  inherited;
  Parent := TWinControl(AOwner);
end;


procedure TGameMech.CreateBoard(w, h, cw, ch, sx, sy: integer);
var
  i, j: integer;
  en: TEnemy;
  ar: TArtifact;
begin
  Width := 78;
  Height := 78;
  PlayerInd := 0;
  PlayerCount := 0;
  EnemyInd := 0;
  EnemyCount := 0;
  TurnsDone := 0;
  XShift := sx;
  YShift := sy;
  SetLength(Characters, MAX_PLAYERS, 2);
  SetLength(CharacterAlive, MAX_PLAYERS);
  SetLength(Enemies, MAX_ENEMIES, 2);
  SetLength(EnemyAlive, MAX_ENEMIES);
  SetLength(TurnDone, MAX_PLAYERS);
  SetLength(Cells, w + w);
  for i := 0 to w - 1 do
  begin
    SetLength(Cells[i], h + h);
  end;
  WidthB := w;
  HeightB := h;
  CellWidth := cw;
  CellHeight := ch;
  SetLength(Cells, w, h);
  for i := 0 to w - 1 do
  begin
    for j := 0 to h - 1 do
    begin
      CreateRandom(sx + i * cw, sy + j * ch, i, j);
      Cells[i][j].UpdateBg := True;
    end;
  end;

  for i := 1 to START_ENEMIES do
  begin
    en := TEnemy(ClassE[random(MaxE)].Create());
    AddEnemy(en, random(w), random(h));
  end;

  for i := 1 to START_ARTIFACTS do
  begin
    ar := TArtifact(ClassA[random(MaxA)].Create());
    Cells[random(w)][random(h)].FArtifact := ar;
  end;

  SetEndGameCell(random(w), random(h));

  CanRefresh := True;
end;

procedure TGameMech.AddCharacter(gamec: TGameCharacter; x, y: integer);
begin
  gamec.ID := PlayerInd;
  Inc(PlayerInd);
  Inc(PlayerCount);
  Characters[gamec.ID][0] := x;
  Characters[gamec.ID][1] := y;
  Cells[x][y].SetCharacter(gamec);
  CharacterAlive[gamec.ID] := True;
  gamec.GMech := TPanel(self);
  gamec.Active := True;
end;

procedure TGameMech.AddEnemy(enemy: TEnemy; x, y: integer);
begin
  enemy.ID := EnemyInd;
  Inc(EnemyInd);
  Inc(EnemyCount);
  Enemies[enemy.ID][0] := x;
  Enemies[enemy.ID][1] := y;
  Cells[x][y].SetEnemy(enemy);
  EnemyAlive[enemy.ID] := True;
  enemy.GMech := TPanel(self);
  enemy.Active := True;
end;

procedure TGameMech.AddArtifact(artifact: TArtifact; x, y: integer);
begin
  Cells[x][y].FArtifact := artifact;
end;

procedure TGameMech.ShiftCharacter(gamec: TGameCharacter; dir: TDir);
var
  cpos: array of integer;
  dx, dy: integer;
begin
  dx := GetDeltaX(dir);
  dy := GetDeltaY(dir);
  cpos := Characters[gamec.ID];
  MoveCharacter(gamec, cpos[0] + dx, cpos[1] + dy);
end;

procedure TGameMech.MoveCharacter(gamec: TGameCharacter; x, y: integer);
var
  cpos: array of integer;
  i: integer;
begin
  if not TurnDone[gamec.ID] then
  begin
    Inc(TurnsDone);
  end;

  cpos := Characters[gamec.ID];
  if (WidthB > x) and (x >= 0) and (HeightB > y) and (y >= 0) and
    CharacterAlive[gamec.ID] and not TurnDone[gamec.ID] then
  begin
    Cells[cpos[0], cpos[1]].SetCharacter(nil);
    cpos[0] := x;
    cpos[1] := y;
    Cells[cpos[0], cpos[1]].SetCharacter(gamec);
    CheckForEndGame;
  end;

  TurnDone[gamec.ID] := True;

  if TurnsDone >= PlayerCount then
  begin
    TurnsDone := 0;
    for i := 0 to MAX_PLAYERS do
    begin
      TurnDone[gamec.ID] := False;
    end;
    DoAITurn;
    ActivateArtifacts;
  end;
end;

procedure TGameMech.DoAITurn;
var
  i, x, y, dx, dy: integer;
  dir: TDir;
begin
  for i := 0 to EnemyInd - 1 do
  begin
    if EnemyAlive[i] then
    begin
      try
        x := Enemies[i][0];
        y := Enemies[i][1];
        dir := Cells[x][y].FEnemy.MakeMove(Characters, CharacterAlive, WidthB, HeightB);
        dx := GetDeltaX(dir);
        dy := GetDeltaY(dir);
        if (WidthB > x + dx) and (x + dx >= 0) and (HeightB > y + dy) and
          (y + dy >= 0) and (Cells[x + dx, y + dy].FEnemy = nil) then
        begin
          Cells[x + dx, y + dy].SetEnemy(Cells[x, y].FEnemy);
          Enemies[i][0] := x + dx;
          Enemies[i][1] := y + dy;
          Cells[x, y].SetEnemy(nil);
        end;
      except
      end;
    end;
  end;
  CheckForEndGame;
end;

procedure TGameMech.ActivateArtifacts;
var
  nPos: TPos;
  x, y: integer;
begin
  for x := 0 to WidthB - 1 do
    for y := 0 to HeightB - 1 do
      if (Cells[x][y].FArtifact <> nil) and (Cells[x][y].FGameC <> nil) then
      begin
        nPos := Cells[x][y].FArtifact.Activate(Cells[x][y].FGameC,
          WidthB, HeightB);
        MoveCharacter(Cells[x][y].FGameC, nPos[0], nPos[1]);
      end;
end;

procedure TGameMech.SetEndGameCell(x, y: integer);
begin
  EndCell[0] := x;
  EndCell[1] := y;
  Left := XShift + x * CellWidth - 4;
  Top := YShift + y * CellHeight - 4;
  Color := clRed;
end;

procedure TGameMech.CheckForEndGame();
var
  i, x, y: integer;
begin
  for i := 0 to EnemyInd - 1 do
  begin
    if EnemyAlive[i] then
    begin
      x := Enemies[i][0];
      y := Enemies[i][1];
      if Cells[x][y].FGameC <> nil then
        KillCharacter(Cells[x][y].FGameC.ID);
    end;
  end;

  for i := 0 to PlayerInd - 1 do
  begin
    if (Characters[i][0] = EndCell[0]) and (Characters[i][1] = EndCell[1]) and
      CharacterAlive[i] then
    begin
      ShowMessage('Player ' + IntToStr(i) + ' has won!');
      DisposeOfCharacter(i);
    end;
  end;
end;

procedure TGameMech.HandleDeath(var Msg: TMessage);
begin
  KillCharacter(msg.WParam);
end;

procedure TGameMech.KillCharacter(id: integer);
begin
  Color := clBlack;
  Left := XShift + Characters[id][0] * CellWidth - 4;
  Top := YShift + Characters[id][1] * CellHeight - 4;
  ShowMessage('Player ' + IntToStr(id) + ' has died!');
  DisposeOfCharacter(id);
  SetEndGameCell(EndCell[0], EndCell[1]);
end;

procedure TGameMech.DisposeOfCharacter(id: integer);
begin
  CharacterAlive[id] := False;
  Cells[Characters[id][0], Characters[id][1]].SetCharacter(nil);
  Inc(PlayerCount, -1);
end;

end.
