unit UFrmMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  TAGraph, Math, UGamePanel, UGameMech, UGameCharacter, std, Types,
  uenemy, udummy, uartifact, TASeries, TASources;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Chart1: TChart;
    Edit1: TEdit;
    Edit2: TEdit;
    Panel1: TPanel;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  p: TGameCell;
  gamec: TGameCharacter;
  en: TEnemy;
  ar: TArtifact;
  gm: TGameMech;
  ser: array[TMagic] of TBarSeries;
  src: array[TMagic] of TListChartSource;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var
  m: TMagic;
  le: integer;
begin
  randomize();
  if gm = nil then
    gm := TGameMech.Create(Form1);

  gm.CreateBoard(BOARD_W, BOARD_H, 75, 75, 210, 20);


  gamec := TGameCharacter(ClassC[random(MaxC)].Create());
  gm.AddCharacter(gamec, random(BOARD_W),
    random(BOARD_H));

  Chart1.ClearSeries;
  le := Ord(High(TMagic)) - Ord(Low(TMagic)) + 1;
  for m := Low(TMagic) to High(TMagic) do
  begin
    src[m] := TListChartSource.Create(Form1);
    src[m].Add(Ord(m), gamec.GetMagic(m));
    ser[m] := TBarSeries.Create(Form1);
    ser[m].Source := src[m];
    ser[m].SeriesColor := RGBToColor(10, 10 + (255 div (2 * le - 1)) * Ord(m), 10);
    ;
    Chart1.AddSeries(ser[m]);
  end;
  Chart1.Update;
  Timer1.Enabled := True;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  gamec := TGameCharacter(ClassC[random(MaxC)].Create());
  gm.AddCharacter(gamec, StrToInt(Edit1.Text) - 1,
    StrToInt(Edit2.Text) - 1);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  gm.SetEndGameCell(StrToInt(Edit1.Text) - 1, StrToInt(Edit2.Text) - 1);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  en := TEnemy(ClassE[random(MaxE)].Create());
  gm.AddEnemy(en, StrToInt(Edit1.Text) - 1,
    StrToInt(Edit2.Text) - 1);

end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
var
  i, k: char;
  m: TMagic;
begin
  if gamec <> nil then
  begin
    k := char(Key);
    if k = 'W' then
      gm.ShiftCharacter(gamec, DUp)
    else if k = 'D' then
      gm.ShiftCharacter(gamec, DRight)
    else if k = 'S' then
      gm.ShiftCharacter(gamec, DDown)
    else if k = 'A' then
      gm.ShiftCharacter(gamec, DLeft);

    for m := Low(TMagic) to High(TMagic) do
    begin
      src[m].Clear;
      src[m].Add(Ord(m), gamec.GetMagic(m));
      Chart1.AddSeries(ser[m]);
    end;
  end;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  if gm <> nil then
    gm.ForcedRefresh;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  if gm <> nil then
  begin
    Timer1.Enabled := False;
    gm.Refresh;
    Timer1.Enabled := True;
  end;
end;

end.
