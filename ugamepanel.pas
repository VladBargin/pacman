unit UGamePanel;

{$mode objfpc}{$H+}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, UGameCharacter, std, UArtifact, uenemy, AnimatedGif, MemBitmap;

type
  TGameCell = class(TPanel)
  public
    FMagic: TMagic;
    FGameC: TGameCharacter;
    FEnemy: TEnemy;
    FArtifact: TArtifact;
    UpdateBg: boolean;
    UpdateChr: boolean;
    constructor Create(AOwner: TComponent); virtual;
    procedure SetCharacter(gamec: TGameCharacter); virtual;
    procedure SetEnemy(enemy: TEnemy); virtual;
    procedure Paint; override;
  protected
    FBitmap: TBitmap;
    FBitMapFile: string;

    procedure LoadBitMap; virtual;
    procedure ChangeMagic; virtual;
  end;

  TClassGameCell = class of TGameCell;

var
  ClassGC: array[0..100] of TClassGameCell;
  MaxGc: integer;

implementation

procedure TGameCell.LoadBitMap();
begin
  FBitmap.LoadFromFile(FBitMapFile);
end;

constructor TGameCell.Create(AOwner: TComponent);
begin
  inherited;
  FBitmap := TBitmap.Create();
  Parent := TWinControl(AOwner);
end;

procedure TGameCell.Paint();
begin

  if UpdateBg then
  begin
    Canvas.Draw(0, 0, FBitmap);
  end;

  if (FArtifact <> nil) then
  begin
    FArtifact.DrawSelf(Canvas);
    UpdateChr := True;
  end;


  if UpdateChr or UpdateBg then
  begin
    if (FEnemy <> nil) then
      FEnemy.DrawSelf(Canvas);

    if (FGameC <> nil) then
      FGameC.DrawSelf(Canvas);
    UpdateBg := False;
    UpdateChr := False;
  end;
  UpdateBg := False;

end;

procedure TGameCell.SetCharacter(gamec: TGameCharacter);
begin
  FGameC := gamec;
  if (FGameC <> nil) and FGameC.Active then
    ChangeMagic;
  UpdateBg := True;
  UpdateChr := True;
end;

procedure TGameCell.SetEnemy(enemy: TEnemy);
begin
  FEnemy := enemy;
  UpdateBg := True;
  UpdateChr := True;
end;

procedure TGameCell.ChangeMagic;
begin
  FGameC.CheckForDeath;
end;

end.
