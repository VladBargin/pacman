unit UCatEnemy;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math, uenemy, std, typinfo;

type
  TCatEnemy = class(TEnemy)
  public
    constructor Create(); override;
    function MakeMove(Characters: TMatrix; CharacterAlive: array of boolean;
      w, h: integer): TDir; override;
  end;

implementation

constructor TCatEnemy.Create;
begin
  inherited;
  FBitMapFile := 'Pictures/cate.bmp';
  LoadBitMap;
end;

function TCatEnemy.MakeMove(Characters: TMatrix; CharacterAlive: array of boolean;
  w, h: integer): TDir;
var
  t : integer;
begin
  t := random(4);
  Result := TDir(t);
end;

initialization
  ClassE[MaxE] := TCatEnemy;
  inc(MaxE);

end.

