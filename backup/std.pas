unit std;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math;

type
  //TMagic = (Fire = 0, Earth, Water, Space, Air, Forest, Lake, Death);
  TMagic = (Death = 0, Forest, Kote);
  TDir = (DUp = 0, DRight, DDown, DLeft);
  TMatrix = array of array of integer;
  TPos = array[0..1] of integer;

function GetDeltaX(dir: TDir): integer;
function GetDeltaY(dir: TDir): integer;

const
  WM_DEAD = WM_USER;
  MAX_PLAYERS = 100;
  MAX_ENEMIES = 100;
  START_ENEMIES = 1;
  START_ARTIFACTS = 2;
  BOARD_W = 14;
  BOARD_H = 9;

implementation

function GetDeltaX(dir: TDir): integer;
begin
  if dir = DRight then
    Result := 1
  else if dir = DLeft then
    Result := -1
  else
    Result := 0;
end;

function GetDeltaY(dir: TDir): integer;
begin
  if dir = DUp then
    Result := -1
  else if dir = DDown then
    Result := 1
  else
    Result := 0;
end;

end.
