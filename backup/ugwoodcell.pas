unit UGWoodCell;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UWoodGameCell, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls, Math;

type
  TGWoodCell = class(TWoodGameCell)
    private
      procedure Paint; override;
  end;
implementation

procedure TGWoodCell.Paint();
var
  Bmp : TBitmap;

begin
  Bmp := TBitmap.Create;
  Bmp.LoadFromFile('a.bmp');
  Bmp.Width := Width;
  Bmp.Height := Height;
  Canvas.Draw(0,0,Bmp);
end;

end.

