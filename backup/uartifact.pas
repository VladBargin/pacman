unit UArtifact;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math, std, AnimatedGif, MemBitmap, UGameCharacter;

type
  TArtifact = class(TObject)
  public
    constructor Create; virtual;
    procedure DrawSelf(Canv: TCanvas);
    function Activate(gamec: TGameCharacter; w, h : integer) : TPos; virtual; abstract;
  protected
    FImg: TAnimatedGif;
    FRect : TRect;

    FGifFile: string;
    procedure LoadGIF; virtual;
  end;

  TClassArtifact = class of TArtifact;

var
  ClassA: array[0..100] of TClassArtifact;
  MaxA: integer;

implementation

constructor TArtifact.Create();
begin
  LoadGIF;
end;

procedure TArtifact.LoadGIF();
var
  w, h : integer;
begin
  FImg := TAnimatedGif.Create(FGifFile); 
  FImg.EraseColor := clBlack;      
  w := 60;
  h := 60;
  FRect.left := 5;
  FRect.top := 5;
  FRect.right := 5 + w;
  FRect.bottom := 5 + h;
end;

procedure TArtifact.DrawSelf(Canv: TCanvas);
begin
  FImg.Update(Canv, FRect);
end;

end.

