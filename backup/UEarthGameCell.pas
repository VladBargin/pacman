unit UEarthGameCell;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, UGamePanel;

Type
  TEarthGameCell = class(TGameCell)
  private
    { Private declarations }
  protected
      procedure Paint; override;
  end;


implementation

procedure TEarthGameCell.Paint;
begin
  inherited;
  Canvas.Brush.Color := RGBToColor(128, 128, 0);
  Canvas.Rectangle (0,0,width, height);
end;

initialization
  ClassGC[MaxGc]:=TEarthGameCell;
  inc(MaxGc);
end.

