unit UTeleportArtifact;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math, std, UArtifact, UGameCharacter;

type
  TTeleportArtifact = class(TArtifact)
    public
      constructor Create; override;
      function Activate(gamec: TGameCharacter; w, h : integer) : TPos; override;
  end;

implementation

constructor TTeleportArtifact.Create;
begin
  FGifFile := 'Pictures/artifact0.gif';
  inherited;
end;

function TTeleportArtifact.Activate(gamec: TGameCharacter; w, h : integer) : TPos;
var
  p : TPos;
begin
  p[0] := random(w);
  p[1] := random(h);
  Result := p;
end;

initialization
  ClassA[MaxA] := TTeleportArtifact;
  inc(MaxA);
end.

