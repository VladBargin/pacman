unit udummy;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls, Math, std;

type
  TDummy = class(TObject)
    public
      Active : boolean;
      GMech : TPanel;
      ID : integer;  
      constructor Create; virtual;
      procedure DrawSelf(Canv : TCanvas);
    protected
      FBitmap : TBitmap;
      FBitMapFile : string;
      procedure LoadBitMap; virtual;
  end;

implementation

constructor TDummy.Create();
begin
  Active := false;
  ID := 0;
  FBitmap := TBitmap.Create;
  FBitmap.TransparentColor := RGBToColor(255, 255, 255);
  FBitmap.Transparent := True;
end;

procedure TDummy.LoadBitMap();
begin
  FBitmap.LoadFromFile(FBitMapFile);
end;

procedure TDummy.DrawSelf(Canv : TCanvas);
begin
  Canv.Draw(10,10,FBitmap);
end;

end.

