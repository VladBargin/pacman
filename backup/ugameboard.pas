unit UGameBoard;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UGamePanel;

type
  TGameBoard = class(TPanel)
    private
      Cells : array of array of TGamePanel;
      FForm : TForm;
      procedure Create(x : integer; y : integer, f : TForm);
      procedure CreateRandom(l, t : integer);
      procedure CreateBoard(x, y : intgeger);
  end;

implementation

procedure TGameBoard.Create(x, y : integer, f : TForm);
var
  i, j : integer;
begin
  FForm := f;
  i := 100;
  while i < f.Width - 70 do
  begin
    j := 0;
    while j <  f.Height - 70 do
    begin
      CreateRandom(i, j);
      j := j + 70;
    end;
    i := i + 70;
  end;
end;

procedure CreateBoard;
var
  i, j : integer;
begin
  FForm := f;
  i := 0;
  while i < x - 70 do
  begin
    j := 0;
    while j <  f.Height - 70 do
    begin
      CreateRandom(i, j);
      j := j + 70;
    end;
    i := i + 70;
  end;
end;

procedure TGameBoard.CreateRandom(l, t : integer);
var
  p : TGamePanel;
begin
    p := ClassGC[random(MaxGc)].Create(Form1);
    p.Left := l;
    p.Top := t;
    p.Width := 70;
    p.Height := 70;
    p.Show;

end.

