program pacman;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, tachartlazaruspkg,
  UFrmMain,
  UGamePanel,
  UForestGameCell,
  upacman,
  ugamemech,
  std,
  UDeathGameCell,
  uartifact,
  uenemy,
  udummy,
  UDeathEnemy,
  UTeleportArtifact,
  UCatEnemy;

{ you can add units after this }

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
