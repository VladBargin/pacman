unit UDeathGameCell;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ugamepanel, Graphics, UGameCharacter, std;

type
  TDeathGameCell = class(TGameCell)
  private
    constructor Create(AOwner: TComponent); override;
  end;

  TDeathGameCellS = class(TDeathGameCell)
  public
    constructor Create(AOwner: TComponent); override;
  protected
    procedure ChangeMagic; override;
  end;

  TDeathGameCellM = class(TDeathGameCell)
  public
    constructor Create(AOwner: TComponent); override;
  protected
    procedure ChangeMagic; override;
  end;

  TDeathGameCellL = class(TDeathGameCell)
  public
    constructor Create(AOwner: TComponent); override;
  protected
    procedure ChangeMagic; override;
  end;

implementation

constructor TDeathGameCell.Create(AOwner: TComponent);
begin
  inherited;
  FMagic := Death;
  LoadBitMap;
end;

constructor TDeathGameCellS.Create(AOwner: TComponent);
begin
  FBitMapFile := 'Pictures/DeathS.bmp';
  inherited;
end;

constructor TDeathGameCellM.Create(AOwner: TComponent);
begin
  FBitMapFile := 'Pictures/DeathM.bmp';
  inherited;
end;

constructor TDeathGameCellL.Create(AOwner: TComponent);
begin
  FBitMapFile := 'Pictures/DeathL.bmp';
  inherited;
end;

procedure TDeathGameCellS.ChangeMagic();
begin
  if FGameC.FMagic = FMagic then
    FGameC.ChangeMagic(FMagic, 10)
  else
  begin
    FGameC.ChangeMagic(FGameC.FMagic, -20);
    FGameC.ChangeMagic(FMagic, -10);
  end;
  inherited;
end;

procedure TDeathGameCellM.ChangeMagic();
begin
  if FGameC.FMagic = FMagic then
    FGameC.ChangeMagic(FMagic, 10)
  else
  begin
    FGameC.ChangeMagic(FGameC.FMagic, -20);
    FGameC.ChangeMagic(FMagic, -10);
  end;
  inherited;
end;

procedure TDeathGameCellL.ChangeMagic();
var
  i : TMagic;
begin
  if FGameC.FMagic = FMagic then
    FGameC.ChangeMagic(FMagic, 15)
  else
  begin
    for i := Low(TMagic) to High(Tmagic) do
      FGameC.ChangeMagic(i, -20)
  end;
  inherited;
end;

initialization
  ClassGC[MaxGc] := TDeathGameCellL;
  ClassGC[MaxGc + 1] := TDeathGameCellM;
  ClassGC[MaxGc + 2] := TDeathGameCellS;
  ClassGC[MaxGc + 3] := TDeathGameCellS;
  Inc(MaxGc, 4);
end.
