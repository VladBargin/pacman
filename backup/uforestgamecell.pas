unit UForestGameCell;

interface

uses
  Classes, SysUtils, ugamepanel, Graphics, UGameCharacter, std;

type
  TForestGameCell = class(TGameCell)
  private
    constructor Create(AOwner: TComponent); override;
  end;

  TForestGameCellS = class(TForestGameCell)
  public
    constructor Create(AOwner: TComponent); override;
  protected
    procedure ChangeMagic; override;
  end;

  TForestGameCellM = class(TForestGameCell)
  public
    constructor Create(AOwner: TComponent); override; 
  protected
    procedure ChangeMagic; override;
  end;

  TForestGameCellL = class(TForestGameCell)
  public
    constructor Create(AOwner: TComponent); override; 
  protected
    procedure ChangeMagic; override;
  end;

implementation

constructor TForestGameCell.Create(AOwner: TComponent);
begin
  inherited;
  FMagic := Forest;
  LoadBitMap;
end;

constructor TForestGameCellS.Create(AOwner: TComponent);
begin       
  FBitMapFile := 'Pictures/ForestS.bmp';
  inherited;
end;

constructor TForestGameCellM.Create(AOwner: TComponent);
begin     
  FBitMapFile := 'Pictures/ForestM.bmp';
  inherited;
end;

constructor TForestGameCellL.Create(AOwner: TComponent);
begin         
  FBitMapFile := 'Pictures/ForestL.bmp';
  inherited;
end;

procedure TForestGameCellS.ChangeMagic();
begin
  if FGameC.FMagic = FMagic then
    FGameC.ChangeMagic(FMagic, 5)
  else
  begin
    FGameC.ChangeMagic(FMagic, -5);
  end;
  inherited;
end;

procedure TForestGameCellM.ChangeMagic();
begin
  if FGameC.FMagic = FMagic then
    FGameC.ChangeMagic(FMagic, 10)
  else
  begin
    FGameC.ChangeMagic(FMagic, -10);
  end;
  inherited;
end;

procedure TForestGameCellL.ChangeMagic();
var
  i : TMagic;
begin
  if FGameC.FMagic = FMagic then
    for i := Low(TMagic) to High(Tmagic) do
        FGameC.ChangeMagic(i, 10)
  else
  begin
    FGameC.ChangeMagic(FMagic, -15);
  end;
  inherited;
end;

initialization
  ClassGC[MaxGc] := TForestGameCellS;
  ClassGC[MaxGc + 1] := TForestGameCellM;
  ClassGC[MaxGc + 2] := TForestGameCellL;
  Inc(MaxGc, 3);
end.
