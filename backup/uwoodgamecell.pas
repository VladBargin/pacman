unit UForestGameCell;

interface

uses
  Classes, SysUtils, ugamepanel, Graphics;

type
  TWoodGameCell = class(TGameCell)
    private
      procedure Paint; override;
  end;
implementation

procedure TWoodGameCell.Paint();
begin
   Canvas.Brush.Color := clGreen;
   Canvas.FillRect(0, 0, Width, Height);
end;

initialization
  ClassGC[MaxGc]:=TWoodGameCell;
  inc(MaxGc);
end.

