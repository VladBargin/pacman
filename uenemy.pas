unit uenemy;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math, udummy, std;

type
  TEnemy = class(TDummy)
  public
    function MakeMove(Characters: TMatrix; CharacterAlive: array of boolean;
      w, h: integer): TDir; virtual; abstract;
  end;

  TClassEnemy = class of TEnemy;

var
  ClassE: array[0..100] of TClassEnemy;
  MaxE: integer;

implementation

end.
