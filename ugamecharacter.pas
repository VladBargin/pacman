unit UGameCharacter;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Windows, Messages, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, Math, udummy, std;

type
  TGameCharacter = class(TDummy)
  public
    FMagic: TMagic;
    function GetMagic(Index: TMagic): integer;
    procedure SetMagic(Index: TMagic; const Value: integer);
    procedure ChangeMagic(Index: TMagic; const Value: integer);
    procedure CheckForDeath;
  protected
    FMagics: array [TMagic] of integer;
  end;

  TClassGameCharacter = class of TGameCharacter;

var
  ClassC: array[0..100] of TClassGameCharacter;
  MaxC: integer;

implementation

function TGameCharacter.GetMagic(Index: TMagic): integer;
begin
  Result := FMagics[Index];
end;

procedure TGameCharacter.SetMagic(Index: TMagic; const Value: integer);
begin
  FMagics[Index] := Value;
end;

procedure TGameCharacter.ChangeMagic(Index: TMagic; const Value: integer);
begin
  FMagics[Index] := FMagics[Index] + Value;
end;

procedure TGameCharacter.CheckForDeath();
var
  i: TMagic;
begin
  for i := Low(TMagic) to High(TMagic) do
  begin
    if GetMagic(i) < 0 then
    begin
      SendMessage(GMech.Handle, WM_DEAD, ID, 0);
      break;
    end;
  end;
end;

end.
