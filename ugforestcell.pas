unit UGForestCell;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UForestGameCell, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls, Math;

type
  TGForestCell = class(TForestGameCell)
    private
      procedure Paint; override;
  end;
implementation

procedure TGForestCell.Paint();
var
  Bmp : TBitmap;

begin
  Bmp := TBitmap.Create;
  Bmp.LoadFromFile('s.bmp');
  Bmp.Width := Width;
  Bmp.Height := Height;
  Canvas.Draw(0,0,Bmp);
end;

end.

