unit UPacman;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, UGameCharacter, Graphics, UGamePanel, std;

type
  TPacman = class(TGameCharacter)
  public
    constructor Create(); override;
  end;

implementation

constructor TPacman.Create();
var
  i: TMagic;
begin
  inherited;

  FMagic := Forest;
  for i := Low(TMagic) to High(TMagic) do
  begin
    FMagics[i] := 100;
  end;

  FBitMapFile := 'Pictures/pac.bmp';
  LoadBitMap;
end;

initialization
  ClassC[MaxC] := TPacman;
  Inc(MaxC);

end.

